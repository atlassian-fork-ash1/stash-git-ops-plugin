# Bitbucket Server Git Operations Plugin

Allows certain git operations to be run in hosted Bitbucket Server repositories, such as initiating garbage collection.

You can initiate this from the repository settings tab in Bitbucket Server, or via a REST endpoint:

    $> curl -u username -X GET -H "Accept: application/json" -H "Content-Type: application/json" http://localhost:7990/bitbucket/rest/git-operations/latest/projects/KEY/repos/SLUG/gc
    
![screenshot](http://monosnap.com/image/ISVzBw19wOLO1NfvLyPuZafgz.png)

# Requirements

This plugin requires Bitbucket Server 4.0 or greater to run. Upgrade or evaluate [Bitbucket Server](http://www.atlassian.com/bitbucket) today!

# Want to contribute?
Thanks! Please [fork this project](https://bitbucket.org/atlassian/stash-git-ops-plugin/fork) and create a pull request to submit changes back to the original project.

### Developer docs

Read up on developing with the [Atlassian plugin SDK](https://developer.atlassian.com/display/DOCS/Developing+with+the+Atlassian+Plugin+SDK) for resources on how to make changes and improve this plugin!
